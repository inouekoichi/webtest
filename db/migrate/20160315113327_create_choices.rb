class CreateChoices < ActiveRecord::Migration
  def change
    create_table :choices do |t|
      t.integer :question_id
      t.integer :choice_id
      t.string :choice_text

      t.timestamps null: false
    end
  end
end
