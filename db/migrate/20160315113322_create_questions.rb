class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :question_id
      t.integer :answer_id
      t.string :question_text
      t.string :code_text

      t.timestamps null: false
    end
  end
end
