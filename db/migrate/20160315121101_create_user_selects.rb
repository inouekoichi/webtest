class CreateUserSelects < ActiveRecord::Migration
  def change
    create_table :user_selects do |t|
      t.string :user_name
      t.integer :count
      t.integer :question_id
      t.integer :select_id
      t.boolean :currect

      t.timestamps null: false
    end
  end
end
