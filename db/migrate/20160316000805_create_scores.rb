class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.string :user_name
      t.integer :count
      t.integer :score

      t.timestamps null: false
    end
  end
end
