class QuestionController < ApplicationController

	def index

		# 問題文を取得
		@question = Question.find_by(question_id: params[:id])
		@choice = Choice.where(question_id: params[:id]).order("choice_id")

		# すでに選択済みの情報があれば
		user_select = UserSelect.find_by(user_name:session[:user_name],count: session[:count],question_id:@question.question_id)
		@user_select_id = user_select ? user_select.select_id : nil

		# ボタン出し分け
		@next = @question.question_id < Q_NUM ? 'NEXT' : 'COMPLETE'
		@back = 'BACK'
		@back_disabled = @question.question_id < 2 ? true : false 

		@total = Q_NUM
	end

	def regist

		question_id = params[:page]['id'].to_i
		
		if !params[:select].blank?
			select_id = params[:select]['id'].to_i
			update_user_select(question_id, select_id)
		else
			flash[:alert] = 'どれか1つを選択してください'
			redirect_to action: 'index', id: question_id
			return
		end

		if params[:button] == 'BACK'
			redirect_to action: 'index', id: question_id - 1
		elsif params[:button] == 'NEXT'
			redirect_to action: 'index', id: question_id + 1
		elsif params[:button] == 'COMPLETE'
			redirect_to controller: 'result', action: 'index'
		end
	end

	private

	def update_user_select(question_id, select_id)

		user_name = session[:user_name]
		count = session[:count]
		currect = select_id == Question.find_by(question_id: question_id).answer_id ? true : false

		# 削除してから更新
		user_select = UserSelect.find_by(user_name:user_name,count: count,question_id:question_id)
		user_select.destroy if user_select.present?

		UserSelect.create(user_name:user_name,count: count,question_id:question_id,select_id:select_id,currect:currect)

	end
end
