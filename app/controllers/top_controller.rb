class TopController < ApplicationController
	# ログインチェックはスキップ
	skip_before_action :check_logined

	def index
		
	end

	# 認証
	def auth
		usr = User.authenticate(params[:username],params[:password])

		logger.debug params[:username]
		puts usr
		if usr then
			save_session(usr)
			redirect_to controller: 'question', action: 'index', id: '1'
		else
			flash[:alert] = 'ユーザー名かパスワードが違います'
			render 'index'
		end
	end

	private

	def save_session(usr)
			reset_session
			session[:id] = usr.id
			session[:user_name] = usr.user_name

			score = Score.where(user_name: usr.user_name)
			if score.any?
				session[:count] = score.maximum(:count) + 1
			else
				session[:count] = 1
			end

			Score.create(user_name: usr.user_name, count:session[:count])
	end
end
