class ApplicationController < ActionController::Base
 
  # 定数
  Q_NUM = 20

  protect_from_forgery with: :exception

  # フィルタ
  before_action :check_logined

  private
  def check_logined

  	if session[:id] then
  		begin
  			@usr = User.find(session[:id])
  		rescue Active::RecordNotFound
  			reset_session
  		end
  	end

  	unless @usr
  		redirect_to '/'
  	end

  end
end
